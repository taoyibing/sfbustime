var geocoder;
var map;
var stoptable = {};

function initialize() {
	geocoder = new google.maps.Geocoder();

	var zoom = 14;
    var latlng = new google.maps.LatLng(37.7830, -122.4170); //Default is San Francisco, CA

    // If ClientLocation was filled in by the loader, use that info instead
    if (google.loader.ClientLocation) {
      latlng = new google.maps.LatLng(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
    } 
	
	var mapOptions = {
	  center: latlng,
	  zoom: zoom
	};
	map = new google.maps.Map(document.getElementById('map-canvas'),
		mapOptions);
	var transitLayer = new google.maps.TransitLayer();
	transitLayer.setMap(map);
	
	google.maps.event.addListener(map,'bounds_changed',boundsChange)
}

function boundsChange()
{
	markStops();
}

function addStop(busStop) {
	var stopTag = busStop.childNodes[1].textContent;
	var lat = busStop.childNodes[3].textContent;
	var lon = busStop.childNodes[5].textContent;
	var latlng = new google.maps.LatLng(lat, lon);

	var marker = new google.maps.Marker({
		  position: latlng,
		  map: map,
		  title: stopTag
	  });

	var infowindow = new google.maps.InfoWindow({
		  
	  });
	
	google.maps.event.addListener(marker, 'click', function() {
		var stopTag = marker.getTitle();	
		var routeList = stoptable[stopTag].getElementsByTagName('route');
		var contentString = "";
		for(i=0;i<routeList.length;i++)
		{
			var agentTag = routeList[i].childNodes[1].textContent;
			var routeTag = routeList[i].childNodes[3].textContent;
			contentString = contentString + '<p>'
			contentString = contentString + routeTag;

			var predicteCommand ='http://sfbustime.appspot.com/bustimeserver?command=prediction&a=' + agentTag + '&r=' + routeTag +'&s=' + stopTag;
			var req = new XMLHttpRequest(); 
			req.open("GET", predicteCommand, false);
			req.send();
			var time =  req.responseXML.getElementsByTagName('time')[0].textContent;
			if(time>0)
			{
				var d = new Date();
				d.setTime(time);
				contentString = contentString + '--' + d.toLocaleTimeString();
			}	
			else
			{
				contentString = contentString + '--unavail</p>';
			}
		}
		infowindow.setContent(contentString);
		infowindow.open(map,marker);
	  });		  
	  
	  stoptable[stopTag]=busStop;
} 

function codeAddress() {
  var address = document.getElementById('address').value;
  geocoder.geocode( { 'address': address}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) {
	  map.setCenter(results[0].geometry.location);
	} else {
	  alert('Geocode was not successful for the following reason: ' + status);
	}
  });
}

var req;
function markStops(url)
{
	req=new XMLHttpRequest(); 
	var url = 'http://sfbustime.appspot.com/bustimeserver?command=getstop'
	var maxlat = map.getBounds().getNorthEast().lat();
	var maxlon = map.getBounds().getNorthEast().lng();
	var minlat = map.getBounds().getSouthWest().lat();
	var minlon = map.getBounds().getSouthWest().lng();
	req.onreadystatechange = processRespnse;
	req.open('GET', url + '&maxlat=' + maxlat + '&minlat=' + minlat + '&maxlon=' + maxlon + '&minlon=' + minlon, true); 
	req.send();
}

function processRespnse()
{
	if(req.readyState == 4 && req.status == 200) 
	{
		var xmlDoc = req.responseXML;
		var stopList = xmlDoc.getElementsByTagName('busStop');
		for(i=0;i<stopList.length;i++)
		{
			var stopTag = stopList[i].childNodes[1].textContent; //stop_tag
			if(!(stopTag in stoptable))
			{
				addStop(stopList[i]);
			}
		}
	}
}