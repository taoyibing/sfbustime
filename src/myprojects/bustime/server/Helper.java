package myprojects.bustime.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;

public class Helper {
	public static String httpGet(String command) throws IOException
	{
		HttpURLConnection connection = null;
		URL url = null;
		
		try
		{
			url = new URL(command);
		
			connection = (HttpURLConnection)url.openConnection();
	        connection.setRequestMethod("GET");
	        connection.setDoOutput(true);
	        
	        connection.connect();
	        try(InputStream stream = connection.getInputStream())
	        {
	        	BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
	        
	        	StringBuilder sb = new StringBuilder();
	            while (true)
	            {
	            	String line = reader.readLine();
	            	if(line == null)
	            		break;
	                sb.append(line);
	            }
				return sb.toString();
	        }
		}
		finally
		{
			connection.disconnect();
		}
	}
	
	public static Document getXMLDocument(String xml) throws IOException
	{
		try
		{
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(xml));
	
		    return builder.parse(is);
		}
		catch(ParserConfigurationException | SAXException ex)
		{
			//TODO: handle exception
			return null;
		}
	}	
	
	private static XStream xstream = new XStream();
	public static String toXML(List<BusStop> busStopList)
	{
		xstream.setMode(XStream.NO_REFERENCES);
		xstream.alias("busStopList", List.class);
		xstream.alias("busStop", BusStop.class);
		xstream.alias("route", Route.class);
		return xstream.toXML(busStopList);
	}
	public static String toXML(Prediction nextBus)
	{
		xstream.alias("prediction", Prediction.class);
		return xstream.toXML(nextBus);
	}
}
