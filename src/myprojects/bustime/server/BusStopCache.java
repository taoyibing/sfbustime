package myprojects.bustime.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;

public class BusStopCache {

	private static final Map<String,BusStop> busStopMap = new HashMap<>();
	private static final String agentListCommand = 
			"http://webservices.nextbus.com/service/publicXMLFeed?command=agencyList";
	private static final String routeListCommand = 
			"http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=%s";
	private static final String routeConfigCommand = 
			"http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=%s&r=%s";
	
	//Free Google App Engine account only support F1 instance which has only 128MB memory. 
	//so limit to 100 route per agency
	private static final int MAX_ROUTE_COUNT = 100;
	
	public static void refresh() 
			throws IOException, ParserConfigurationException, SAXException, InterruptedException
	{
		String agentList = Helper.httpGet(agentListCommand);
		
		Document agentListDocument = Helper.getXMLDocument(agentList);
		NodeList agentNodeList = agentListDocument.getElementsByTagName("agency");
		// each agency
		for(int i=0;i<agentNodeList.getLength();i++)
		{
			int routeCount = 0;
			
			String agentTag = agentNodeList.item(i).getAttributes().getNamedItem("tag").getNodeValue();
			String agentTitle = agentNodeList.item(i).getAttributes().getNamedItem("title").getNodeValue();
			String routeList = Helper.httpGet(String.format(routeListCommand, agentTag,agentTitle));
			Document routeListDocument = Helper.getXMLDocument(routeList);
			NodeList routeNodeList = routeListDocument.getElementsByTagName("route");
			
			for(int j=0;j<routeNodeList.getLength();j++)
			{
				routeCount++;
				String routeTag = routeNodeList.item(j).getAttributes().getNamedItem("tag").getNodeValue();
				Route route = new Route();
				route.agency_tag=agentTag;
				route.route_tag=routeTag;
				
				String routeStopList = Helper.httpGet(String.format(routeConfigCommand, agentTag,routeTag));
				Document routeStopListDocument = Helper.getXMLDocument(routeStopList);
				NodeList routeStopNodeList = routeStopListDocument.getElementsByTagName("route");
				if(routeStopNodeList.getLength()>0)
				{
					NodeList childList = routeStopNodeList.item(0).getChildNodes();
					for(int k=0;k<childList.getLength();k++)
					{
						Node node = childList.item(k);
						BusStop stop;
						if(node.getNodeName().equals("stop"))
						{
							if(busStopMap.size()>1000)
								return;
							if(!busStopMap.containsKey(node.getAttributes().getNamedItem("tag").getNodeValue()))
							{
								stop = new BusStop();
								stop.stop_tag = node.getAttributes().getNamedItem("tag").getNodeValue();
								stop.latitude = Float.parseFloat(node.getAttributes().getNamedItem("lat").getNodeValue());
								stop.longitude = Float.parseFloat(node.getAttributes().getNamedItem("lon").getNodeValue());
								stop.routeList = new ArrayList<>();
								stop.routeList.add(route);
								busStopMap.put(stop.stop_tag,stop);
							}
							else
							{
								stop = busStopMap.get(node.getAttributes().getNamedItem("tag").getNodeValue());
								stop.routeList.add(route);
							}
						}
					}//stop
				}
				if(routeCount>MAX_ROUTE_COUNT)
					break;
			}//route
		}//agency
	}
	
	public static List<BusStop> getStopList(float minLat, float maxLat, float minLon, float maxLon)
	{
		List<BusStop> result = new ArrayList<>();
		
		for(BusStop stop : busStopMap.values())
		{
			if(stop.latitude<maxLat && stop.latitude>minLat
				&& stop.longitude<maxLon && stop.longitude>minLon)
				result.add(stop);
		}
		return result;
	}
}
