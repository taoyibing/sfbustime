package myprojects.bustime.server;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.*;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;

@SuppressWarnings("serial")
public class BusTimeServerServlet extends HttpServlet 
{
	public void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws IOException 
	{
		String command = req.getParameter("command").toLowerCase();
		switch (command)
		{
			case "healthcheck":
				processHealthCheck(req,resp);
				break;
			case "getstop":
				processGetStop(req,resp);
				break;
			case "prediction":
				processGetNextBus(req,resp);
				break;
		}
	}
	
	private void processHealthCheck(HttpServletRequest req, HttpServletResponse resp) 
			throws IOException
	{
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
	}
	
	private void processGetStop(HttpServletRequest req, HttpServletResponse resp) 
			throws IOException
	{
		float minLat = Float.parseFloat(req.getParameter("minlat"));
		float maxLat = Float.parseFloat(req.getParameter("maxlat"));
		float minLon = Float.parseFloat(req.getParameter("minlon"));
		float maxLon = Float.parseFloat(req.getParameter("maxlon"));
		
	    resp.setHeader("Content-Type", "application/xml");
	    resp.getWriter().append(Helper.toXML(BusStopCache.getStopList(minLat, maxLat, 
	    		minLon, maxLon)));
	}
	
	private void processGetNextBus(HttpServletRequest req, HttpServletResponse resp) 
			throws IOException
	{
		final String predictCommand = 
				"http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=%s&r=%s&s=%s";

		String agentTag= req.getParameter("a");
		String routeTag= req.getParameter("r");
		String stopTag= req.getParameter("s");
		
		String nextBus = Helper.httpGet(String.format(predictCommand, agentTag,routeTag,stopTag));
		Document doc = Helper.getXMLDocument(nextBus);
		NodeList predictionNodeList = doc.getElementsByTagName("prediction");

		Prediction prediction = new Prediction();
		if(predictionNodeList.getLength()>0)
		{
			prediction.time = Long.parseLong(
					predictionNodeList.item(0).getAttributes().getNamedItem("epochTime").getNodeValue());
		}
	    resp.setHeader("Content-Type", "application/xml");
	    resp.getWriter().append(Helper.toXML(prediction));
	}
}
