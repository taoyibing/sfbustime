package myprojects.bustime.server;

import java.io.IOException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

@SuppressWarnings("serial")
public class WarmUpServlet extends  GenericServlet{

	@Override
	public void service(ServletRequest arg0, ServletResponse arg1)
			throws ServletException, IOException {
	}
	
	@Override
	public void init()
	{
		try {
			BusStopCache.refresh();
		} catch (IOException | ParserConfigurationException | SAXException
				| InterruptedException ex) {
			// TODO: handle exception
		}
	}

}
