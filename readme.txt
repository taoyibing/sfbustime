This application provide a simple web service and map application to show real time bus departure time for San Francisco area.

A simple java web service provide 3 commands
1. http://sfbustime.appspot.com/bustimeserver?command=healthcheck 
   Just check web service availibilitiy
2. http://sfbustime.appspot.com/bustimeserver?command=getstop&minlat=37.84408&maxlat=37.84410&minlon=-122.27492&maxlon=-122.27490  
   Get a list of bus stops with in the area
3. http://sfbustime.appspot.com/bustimeserver?command=prediction&a=sf-muni&r=N&s=5205
   Get the next bus time for the agency/route/stop
   
During service warmup, the service will try to cache all bus stops in SF area, using http://webservices.nextbus.com/service/publicXMLFeed
Due to Google app engine limitation (for free account), the service only cache up to 1000 stops

The map application is a javascript based simple google map API. it will load map based on user IP address or user can input the target city. 
And it will mark the bus stop on the map. If the user click the stop marker, it will show next bus time on user local time, or unavail if no
next bus available.